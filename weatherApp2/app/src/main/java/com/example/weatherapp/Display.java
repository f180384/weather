package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;


public class Display extends AppCompatActivity {

    String City;
    Button main;
    String url = "http://api.weatherstack.com/current";
    String key = "09eabf22ef28349d80d6a879778cb186";
    String req1 = "";
    TextView locate;
    TextView wDesc;
    ImageView icon_img;
    TextView tempe;
    TextView windv;
    TextView precv;
    TextView presv;
    TextView feel;



    DecimalFormat df = new DecimalFormat("#.##");

    //"http://api.weatherstack.com/current?access_key=09eabf22ef28349d80d6a879778cb186&query=New%20York"
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        Intent intent = getIntent();
        City = intent.getStringExtra("City");

        main = findViewById(R.id.main);
        icon_img =findViewById(R.id.iconw);

        locate =findViewById(R.id.locaw);
        wDesc =findViewById(R.id.descw);
        tempe =findViewById(R.id.tempw);
        windv =findViewById(R.id.windw);
        precv =findViewById(R.id.precew);
        presv =findViewById(R.id.presw);
        feel = findViewById(R.id.feelw);



        req1 = url + "?access_key=" + key + "&query=" + City;


        getData();

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Main_Int = new Intent(Display.this, MainActivity.class);
                startActivity(Main_Int);
                finish();
            }
        });
    }


    Response.Listener<JSONObject> successBLock = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {

            try {
                JSONObject jsonResponse = response;
                JSONObject current = jsonResponse.getJSONObject("current");
                JSONObject location = jsonResponse.getJSONObject("location");
                JSONArray des = current.getJSONArray("weather_descriptions");
                String desc = des.getString(0);
                JSONArray ic = current.getJSONArray("weather_icons");
                String icon = ic.getString(0);
                double temp = current.getDouble("temperature");
                double feelsLike = current.getDouble("feelslike");
                float pressure = current.getInt("pressure");
                int preci = current.getInt("precip");
                String wind = current.getString("wind_speed");
                String countryName = location.getString("country");
                String cityName = location.getString("name");
                locate.setText(cityName+", "+countryName);
                tempe.setText(df.format(temp) + " °C");
                Picasso.get().load(icon).into(icon_img);
                wDesc.setText(desc);
                windv.setText("Wind:" + wind + "kmph");
                presv.setText(" Pressure: " + pressure +  "mb");
                presv.setText("Precip"+ preci);
                feel.setText("Feels Like:" + df.format(feelsLike) + " °C");

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(Display.this, "No data", Toast.LENGTH_SHORT).show();
            }
        }
    };
    Response.ErrorListener failureBlock = new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(Display.this,error.toString(), Toast.LENGTH_SHORT).show();
        }


    };
    private void getData()
    {
        String link = req1;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,link,null,successBLock,failureBlock);
        Volley.newRequestQueue(this).add(request);
    }

}
